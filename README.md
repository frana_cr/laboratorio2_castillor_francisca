# Laboratorio 2, Unidad 2 : " Creacion de procesos con fork() "

## Comenzando 
### --- EXPLICACION PROBLEMATICA ---
La problematica del segundo laboratorio de la unidad 2 , es que se pide que por medio de la implementacion de Fork () se genere la creacion de subprocesos, en lenguaje C++, que permita al usuario segun una URL de un video en youtube, poder descargarlo y extraer el audio en un formato MP3, para poder luego reproducir el audio por medio de un reproductor desde la terminal.

### ---- COMO SE DESARROLLO ----
Para poder resolver la problematica de la guia lo primero que se intenta hacer es poder comprender como funciona la herramienta fork(), de esta manera se puede implementar su uso en la solucion. Luego se procedio a diseñar el modelo de la resolucion de las problematicas definiendo las funciones que pueden ser las mas eficientes y capaces de buscar una clara solucion al problema. Se descarga un video desde la plataforma Youtube (utilizando en este caso youtube-dl) por medio de la URL de este para luego extraer solamente un audio y reproducirlo desde manera directa al descargar por medio de la terminal. La reproduccion del audio se lleva a cabo con Totem, se decidio trabajar con este reproductor porque es mucho mas amigable con GNOME y permite la reproduccion de video y audio, lo que se necesitaba esta vez y en el formato .MP3, ademas esta en las distribuciones de Debian/ Linux y Fedora. Se probo con Rhythmbox y VLC pero la solucion no fue tan optima como con Totem y a su vez no se lograba llevar a cabo la reproduccion. Los archivos principales de la solucion del problema son detallados a continuacion con su funcionamiento y caracteristicas propias.

- Makefile:  Permite que se pueda compilar el codigo mediante la terminal y desarrollar las pruebas de ejecucion en donde se indican los posibles errores del codigo y a la vez indica si la compilacion se ha desarrollado de manera exitosa. Es un requerimiento imprescindible dentro de los programas para poder ver su funcionalidad.

- youtubeReproductor.cpp: En este archivo principal en donde se llevan a cabo la funcionalidad del programa la cual puede ser probada y ademas se lleva a cabo la interaccion con el usuario. Se incluyen las librerías a implementar y las funciones basicas  para que se lleve a cabo el programa. 
Lo que contiene el archivo son las siguientes funciones:

    - _ForkDescargaVideo:_ Esta es la clase del programa, contiene informacion sobre los atributos privados como publicos cada uno tiene distintos parametros. Por ejemplo los _atributos privados_ contiene el parametro necesario para implementar fork (permite crear subprocesos en un programa) tambien tiene el parametro en donde se asigna la direccion del video que se descarga. En los _atributos publicos_ contiene el constructor, en donde se llama a la clase del programa, la direccion es igualada a la direccion del video y se llama a la funcion que creara el proceso por medio de la implementacion de fork y ademas de la descarga y obtencion del audio _crearPrcoesoDescarga()_ y _ProgramaDescargaAudio_ las cuales se explicaran con mayor detalle a continuacion.

    - _crearPrcoesoDescarga():_ Esta funcion es permite poder crear el proceso hijo por medio de fork que es fundamental para que se ejecute el proceso de la descarga y obtencion del audio.

    - _ProgramaDescargaAudio_ : La siguiente funcion es la encargada de llevar a cabo la descarga y procesamiento del audio por medio de unas simples condiciones. Por ejemplo si el pid definido en el atributo privado es menor a 0, el proceso no se crea con exito y por lo tanto la descarga no puede ser realizada. 
        - Si el pid es igual al valor 0 se imprime la direccion que el usuario ingresa del video que desea obtener el audio , tambien se comienza a llevar a cabo el proceso usando execlp para poder realizar la descarga del audio esto es muy importante porque le permite conectarse a una ruta y obtener ciertos atributos que permitiran descargar el archivo por medio del uso y la implementacion de youtube-dl (un programa de la linea de comandos que permite la descarga de un video y su posterior extraccion del audio). Algunos de estos atributos que van en la linea de comando se investigaron y se probaron para ver cual de estos era el mas indicado, en donde se necesita el nombre de youtube-dl, algunos parametros extra para la extraccion, el nuevo formato que se requiere (.MP3 en este caso) y tambien el nombre de salida que tendra el audio-cancion del video. Este proceso de ejecuta de manera correcta descargando de buena manera el video.
    
        - Posteriormente existe un else ante las condiciones establecidas anteriormente, en donde por el uso de fork termina el proceso del padre y comienza el proceso del hijo (subproceso), se imprime que el proceso es terminado, tambien un mensaje que dice que se reproducira el audio - cancion y su formato .MP3 esto es debido a que dice en el laboratorio que se extraiga el audio del video en este formato. Se incluyen ademas las lineas de comando por donde usando execlp se incluye totem (reproductor elegido) para la reproduccion de manera directa del archivo generado en la descarga el cual contiene el nombre de cancion_descarga.mp3 la cual se reproducira automaticamente al llevar a cabo todo el programa. El uso de totem se priorizo porque fue mucho mas facil su implementacion e instalacion.

    - _int main_: Esta es la funcon principal de todo el programa y el que permite recopilar las funciones anteriores y ejecutarlas para que se lleve a cabo la descarga y extraccion del audio.

        - Se define el numero de arreglos que tendran los parametros, tambien cuantos caracteres puede tener la direccion del video (en este caso se dio un tope de hasta 500), se ejecutan unas condiciones que permiten evaluar el ingreso del valor del parametro en donde si es distinto de 2 imprime el mensaje al usuario de que el parametro ingresado es erroneo. De manera contraria se imprime un mensaje en donde se indica que la cantidad es valida y se ejecutan las funciones mencionadas con anterioridad, se hace uso de Strcpy porque permite de esta manera que cuando el usuario ingrese la direccion este pasara a tener un valor de parametro 1. 
        - Se llama a la clase principal en donde esta tendra el parametro de la direccion que ingrese el usuario y se comienze a realizar el proceso de descarga y reproduccion del audio.

Luego de que el usuario compruebe su funcionamiento por medio de la terminal (./youtubeReproductor link_video) se concluye que el programa se ejecuta con exito sin problematicas, se puede ingresar el link del video, se corrobora el parametro ingresado cumpla con las condiciones (sea solamente 1) y se muestra la direccion del video a descargar. Tambien luego se muestra un comentario que se esta empezando la descarga, se observa como se descarga el video por medio de youtube-dl, luego termina el proceso y se imprime un mensaje que indica que se reproducira el audio-cancion del archivo, la cual tendra el nombre cancion_descarga.mp3 y se abrira de manera automatica el reproductor de musica comenzando a sonar. En donde se puede escuchar de buena manera la cancion-audio pudiendo realizar acciones como pausar, acelerar, retroceder, subir el volumen y cerrar sin dificultades. Al finalizar la ejecucion se puede revisar que el audio esta guardado en la carpeta del laboratorio.

## Prerequisitos

- Sistema operativo Linux versión igual o superior a 18.04
- Editor de texto (atom o vim)
- Instalacion de youtube-dl (Descargar video)
- Instalacion de Totem (reproductor)

## Instalacion
1. Para poder ejecutar los programa se debe conocer que versión de Ubuntu presenta la computadora. Ejecutar este comando:

    - _lsb_release -a (versión de Ubuntu)_

     _En donde:_ Se puede corroborar qué versión se tiene instalada actualmente, debido a que se pueden presentar problemas si esta no es compatible   con la aplicación que se está desarrollando. 

2. Para descargar un editor de texto como vim se puede descargar de la siguiente manera:

    - _sudo apt install vim_

     _En donde:_ Por medio de este editor de texto se puede construir el codigo. 

     Tambien si se desea ocupar un editor de texto diferente a vim este se puede instalar por la terminal o por el centro de software en donde se escribe el editor que se desea conseguir y luego se ejecuta la descarga.

- En el caso del desarrollo del trabajo se implemento el editor de texto Atom, descargado de Ubuntu Software.

3. Para instalar youtube-dl se ejecuta por medio de la terminal, en este caso de linux por medio de los siguientes comandos:
    - _sudo apt-get update_

     _En donde:_ Permite actualizar la lista de los paquetes que hay en la computadora con sus respectivas versiones, se ejecuta en caso de que yotube-dl ya estuviera instalado previamente para que se actualice a su version mas reciente de esta manera se pueden disminuir errores con su ejecucion.

    - _sudo apt-get install youtube-dl_

     _En donde:_ Se instala de manera directa y rapida por medio de la terminal el programa, lo unico que se requiere luego de ingresar el comando es agregar la contraseña del usuario y confirmar la instalacion con la letra S que puede estar en minuscula como mayuscula.

Estos comandos varian segun los sistemas operativos que se ocupen, en este caso funcionaron de manera correcta en Linux, para mayor informacion se puede visitar la pagina: https://ytdl-org.github.io/youtube-dl/download.html

4. Para poder instalar el reproductor de audio escogido totem en este caso, se ejecuta el siguiente comando: 
    - _sudo apt-get update_

     _En donde:_ Se actualizan la lista de paquetes que estan en la distribucion (Linux en este caso), no se instalan nuevos paquetes, solo se corrobora los que estan. De esta manera si Totem ya estaba en la distibucion que es lo mas probable porque es parte de GNOME se actualiza para que cuando se use no tenga mayores inconvenientes.

     Posteriormnete se realiza el comando:
     - _sudo apt-get install totem_

     _En donde:_ Se instala por la terminal el reproductor totem que permitira escuchar el audio (.mp3) del video. Luego se tiene que ingresar la contraseña del usuario y aceptar si es que se desea continuar ingresando la letra indicada (S o s).


#### INSTALACION MAKEFILE
Para poder llevar a cabo el funcionamiento del programa se requiere la instalacion del Make para esto primero hay que revisar que este en la computadora, para corroborar se debe ejecutar este comando:

- _make_

De esta forma se puede ver si esta instalado para luego llevar a cabo el desarrollo del programa, si este no esta instalado, se tiene que insertar el siguiente comando en la terminal de la carpeta del programa.

- _sudo apt install make_

_En donde:_ se procede a la creacion del make para que luego este se lleve a cabo y se pueda observar la construccion del archivo.

Luego de instalar el Make, para poder ejecutar las pruebas se tiene que en la terminal entrar en la carpeta donde se encuentran los archivos mediante el comando cd con el nombre de dicha carpeta, luego ver si estan los archivos correctos o vizualizar lo que contiene la carpeta se debe ingresar el comando ls, finalmente se debe escribir make en la terminal con el siguiente comando:

- _make_

## Ejecutando Pruebas
Cada archivo se puede ejecutar para ver si tiene errores por medio del comando en terminal g++ Nombre del archivo.cpp -o Nombre Archivo. o por medio de un archivo Makefile donde se reunen las condiciones para hacer funcionar el programa
Para poder ejecutar el programa se tiene que hacer las siguientes condiciones:
Se debe ingresar a la carpeta donde se esta trabajando con los archivos del programa y esta debe contener el archivo Makefile, si este no ha sido probado con anterioridad luego se agrega el comando:

- _make_

De esta manera se esta compilando el trabajo, si este no presenta comentarios significa que la prueba finalizo con exito.
Por otra parte si se esta probando con el comando G++ se tiene que hacer el siguiente comando por terminal:

- _g++ YoutubeReproductor.cpp -o YoutubeReproductor_

Si la carpeta con archivos presenta otras extensiones diferentes a .cpp, .h o al archivo makefile se debe ejecutar el siguiente comando:

- _make clean_

Posteriormete se ejecuta en la terminal

- _make_

Luego se lleva a cabo el programa ejecutando el siguiente comando por terminal:
- _./youtubeReproductor Linkdelvideo_ 

(*)El link del formato puede ser copiado de manera directa por el nombre de dominio (URL) o cabecera de la pagina de Youtube o tambien haciendo click derecho en el video seleccionado la opcion copiar URL del video.

## Construido con:

- Ubuntu: Sistema operativo.
- C++: Lenguaje de programación.
- Atom: Editor de código.
- Youtube-dl : Descarga del video.
- Totem: Reproductor de audio 


## Versiones
- Versiones de herramientas:
Ubuntu 20.04 LTS
Atom 1.57.0
Versiones del desarrollo del codigo: https://gitlab.com/frana_cr/laboratorio2_castillor_francisca
## Autores
Francisca Castillo - Desarrollo del código y proyecto, narración README.

## Expresiones de gratitud
A los ejemplos en la plataforma de Educandus de Alejandro Valdes: https://lms.educandus.cl/mod/lesson/view.php?id=569958&pageid=8306
Tambien al ayudante del modulo por aclaracion de dudas y a la informacion obtenida algunas paginas web que se usaron para guiar el proceso las cuales son:
- https://es.wikipedia.org/wiki/Strcpy
- https://zoomadmin.com/HowToInstall/UbuntuPackage/totem
- https://www.it-swarm-es.com/es/c/no-entiendo-como-funciona-execlp-en-linux/1043641237/
- https://github.com/ytdl-org/youtube-dl/issues/6724
- https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwiSiZOtmpHxAhV2IbkGHUioBj0QFjADegQIAhAD&url=https%3A%2F%2Fwww.it-swarm-es.com%2Fes%2Fc%252B%252B%2Fcomo-reproducir-o-abrir-el-archivo-de-sonido-.mp3-o-.wav-en-el-programa-c%2F1044921370%2F&usg=AOvVaw0JfO8WD0hAZ2BlHWEV0nKd
- https://www.youtube.com/watch?v=O8CD2CIhQO8 
- http://manpages.ubuntu.com/manpages/bionic/es/man2/fork.2.html
- https://beasthackerz.ru/es/programmy/man-execlp-3-zapusk-faila-na-ispolnenie.html
- https://www.qnx.com/developers/docs/7.0.0/#com.qnx.doc.neutrino.lib_ref/topic/e/execlp.html
- https://askubuntu.com/questions/1158103/problem-with-mp3-tags-using-youtube-dl-and-id3v2
