prefix=/usr/local
CC = g++

CFLAGS = -g -Wall 
SRC = youtubeReproductor.cpp
OBJ = youtubeReproductor.o
APP = youtubeReproductor

all: $(OBJ)
	$(CC) $(CFLAGS)-o $(APP) $(OBJ)

clean:
	$(RM) $(OBJ) $(APP)

install: $(APP)
	install -m 0755 $(APP) $(prefix)/bin

uninstall: $(APP)
	$(RM) $(prefix)/bin/$(APP)
