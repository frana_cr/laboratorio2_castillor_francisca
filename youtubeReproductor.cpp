// Este es el comando para la ejecucion del programa por la terminal.
/*
 * $ g++ YoutubeReproductor.cpp -o YoutubeReproductor
 */

// Se incluyen las librerias a utilizar en el programa
#include <unistd.h> // Libreria para trabajar con execlp
#include <sys/wait.h> // Libreria utilizada para pid_t
#include <iostream> // Libreria estandar de C++
#include <string.h> // Libreria utilizada strcpy

// Por medio de esta opcion sacamos los std:: del cout y endl.
using namespace std;

// Se hace la clase del fork para la descarga del video para luego convertirlo en audio
class ForkDescargaVideo {
	// Atributos privados
	private:
		// Parametro necesario para utilizar el fork
		pid_t pid;
		// Paramatro que se asigna y que pasamos para tener la direccion para descargar el video
		char *direccion_video_descargar;

	// Atributos publicos
	public:
		// Constructor
		// Se llama a la clase del programa
		ForkDescargaVideo(char *video_descarga_url) {
			this->direccion_video_descargar = video_descarga_url;
			// Llama a la funcion para crear el proceso
			crearProcesoDescarga();
			// Se llama a la funcion que hara el programa
			ProgramaDescargaAudio(direccion_video_descargar);
		}
		// Esta funcion es de tipo void y permite crear el proceso de la descarga con fork (Metodos)
		void crearProcesoDescarga() {
			// Se crea el programa hijo
			pid = fork();
		}
	 // Esta funcion es la encargada de llevar a cabo el programa de descarga del audio
		void ProgramaDescargaAudio (char *direccion_video_descargar) {
			// Se valida la creación de proceso hijo, si el pid es menor a 0
			if (pid < 0) {
				// El proceso no se crea con exito
				cout << " NO SE CREO EL PROCESO, ERROR " << endl;
			}
			// Si la condicion de pid es igual a 0
			else if (pid == 0) {
				// Se imprime la direccion ingresa por el usuario para descargar
				cout  << "La direccion del video a descargar es: " << this->direccion_video_descargar << endl;
				cout << endl;
				// Se empieza a ejecutar el proceso hijo (Fork)
		  	// Con este comando se descarga el video de manera externa (por un ejecutable) por Youtube-dl
		    // Se define ciertos parametros que permiten la descarga definiendo el formato (mp3) y como se llamara el archivo que se descargara
				execlp ("youtube-dl","--verbose","--extract-audio","--audio-format","mp3","-o","cancion_descarga.mp3", this->direccion_video_descargar, NULL); // programa usado
			}
			// Si ninguna de las condiciones se cumple
			else {
				// Este comando es parte del pid_d del fork y es igual a NULL
				wait (NULL);
				// Se termina el codigo del proceso hijo y continua el padre (Fork)
				cout << " PROCESO TERMINADO" << endl;
				// Se imprime que se reproducira la cancion y el audio de la descarga
				cout << "--------------------------------------------------" << endl;
				cout << " REPRODUCIENDO CANCION - AUDIO DEL VIDEO EN MP3 " << endl;
				cout << "--------------------------------------------------" << endl;
				// Con este comando se usa se conecta de manera externa con execlp y el reproductor
				// El reproductor de audio ocupado es TOTEM (luego de probar con otros reproductores se decidio por este porque porque es parte de GNOME-Linux), se ingresa el nombre de la cancion y del reproductor
				execlp("totem","totem","cancion_descarga.mp3", NULL); // Se reproducira con el audio de la descarga
			}
		}
	};

// Esta es la funcion principal para la ejecucion del programa y obtener un audio
int main (int argc, char  *argv[]) {
	// Esta funcion define que es 1 el arreglo que tendra los parametros
 char *parametro = argv[1];
 // Se define que cuando el usuario ingrese la direccion del video este sera un char y hasta 500 caracteres
 char direccion_video[500];
 // Se generan condiciones para ver la cantidad de PARAMETROS
 // Si el parametro es distinto e igual a 2
 if (argc != 2){
	 		// Se imprime que la cantidad de parametros es erronea
     cout << " CANTIDAD INCORRECTA DE PARAMETROS, ERROR! " << endl;
 }
 // De modo contrario se cumplira la siguiente condicion
 else {
	 // Se imprime que la cantidad de parametros es correcta
	 cout << " Cantidad correcta de parametros " << endl;
	 // Se imprime que el video se esta descargando y extrayendo el audio
   cout << "--------------------------------------------------" << endl;
   cout << " DESCARGANDO VIDEO Y EXTRAYENDO AUDIO" << endl;
   cout << "--------------------------------------------------" << endl;
	 // Por medio de esta funcion la variable de la direccion tendra el valor del parametro en este caso es 1
   strcpy(direccion_video, parametro);
	 // Se llama a la clase principal
   ForkDescargaVideo ForkDescargaVideo(direccion_video);
   return 0;
 }
}
